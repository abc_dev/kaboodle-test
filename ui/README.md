# UI

Main UI layer

## Stack

- Typescript
- NextJS
- react-hook-form

## Missing

- Lots....
- Proper designs as what I did here https://www.figma.com/file/viT63nxzS1fa6WkQGlbdWN/kaboodle-designs-alan-ionita?type=design&node-id=0%3A1&mode=design&t=rHKq9HxApxcU2mKK-1
- Accessibility
- Animation
- Styling
- E2E tests
- Cypress tests

## Future

- Build the designs
- Styling:
    - Likely use css modules as they are here
    - Plus add components from MUI or Shadcn where needed 
    - Horizontal scroller I'd do in plain css because of accessibility
- Some motion and parallax would've been nice
