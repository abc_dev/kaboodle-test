"use strict";
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __copyProps = (to, from, except, desc) => {
  if (from && typeof from === "object" || typeof from === "function") {
    for (let key of __getOwnPropNames(from))
      if (!__hasOwnProp.call(to, key) && key !== except)
        __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  }
  return to;
};
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: true }), mod);

// functions/function.ts
var function_exports = {};
__export(function_exports, {
  handler: () => handler
});
module.exports = __toCommonJS(function_exports);
var import_aws_sdk = require("aws-sdk");
var dynamo = new import_aws_sdk.DynamoDB.DocumentClient();
var TABLE_NAME = process.env.TABLE_NAME;
var handler = async (event, context) => {
  const method = event.requestContext.http.method;
  if (method === "GET") {
    return await getHello(event);
  } else if (method === "POST") {
    return await save(event);
  } else {
    return {
      statusCode: 400,
      body: "Not a valid operation"
    };
  }
};
async function save(event) {
  const name = event.queryStringParameters.name;
  const item = {
    name,
    date: Date.now()
  };
  console.log(item);
  const savedItem = await saveItem(item);
  return {
    statusCode: 200,
    body: JSON.stringify(savedItem)
  };
}
async function getHello(event) {
  const name = event.queryStringParameters.name;
  const item = await getItem(name);
  if (item !== void 0 && item.date) {
    const d = new Date(item.date);
    const message = `Was greeted on ${d.getDate()}/${d.getMonth() + 1}/${d.getFullYear()}`;
    return {
      statusCode: 200,
      body: JSON.stringify(message)
    };
  } else {
    const message = "Nobody was greeted with that name";
    return {
      statusCode: 200,
      body: JSON.stringify(message)
    };
  }
}
async function getItem(name) {
  const params = {
    Key: {
      name
    },
    TableName: TABLE_NAME
  };
  return dynamo.get(params).promise().then((result) => {
    console.log(result);
    return result.Item;
  });
}
async function saveItem(item) {
  const params = {
    TableName: TABLE_NAME,
    Item: item
  };
  return dynamo.put(params).promise().then(() => {
    return item;
  });
}
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  handler
});
